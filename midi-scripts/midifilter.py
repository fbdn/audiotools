# from mididings import *
import os



def method_list(c, filter="__"):
    return [
            f for f in dir(c) 
            if callable(getattr(c, f)) 
            and not filter in f
            ]

class MidiFilter:
    def __init__(self, dev_name, backend="alsa", ports=1):
        self.name = __file__.strip(".py")
        self.backend = backend
        self.ports = ports
        self.dev_name = dev_name
        self.dev_port_filter = "aplaymidi -l|grep '%s'|awk '{print $1}'" % self.dev_name
        self.dev_name_filter = "aplaymidi -l|grep '%s'|awk '{for(i=3;i<=NF;++i)printf $i " "}'|sed -e 's; $;;g'".strip("\n")  % self.dev_name


        self.dev_num = os.popen(self.dev_port_filter).read().strip("\n")
        self.dev_name = os.popen(self.dev_name_filter).read()

        if self.dev_num is "":
            print "Your device does not seem to be connected:", self.dev_name
            exit
        print "Found dev:", self.dev_name, self.dev_num

        config(
            backend = self.backend, 
            client_name = self.name, 
            in_ports = [(self.dev_name,self.dev_num)], 
            # out_ports = self.ports)
            )

    def fix_velocity(self, velocity):
            # Dont ask me, anything from here is black magic...
            port_filter = PortFilter(self.dev_name) >> \
                          Filter(NOTE|PROGRAM|AFTERTOUCH) >> \
                          Velocity(fixed=velocity) >> \
                          Print('<') >> \
                          Output(1, 1) >> \
                          Print('>') 
            run(port_filter)

    def monitor(self):
        num = int(self.dev_num.split(":")[0])
        port_filter = PortFilter(self.dev_name) >> \
                Filter(NOTE|PROGRAM|AFTERTOUCH) >> \
                Print('<')# >> Port(num) >> Print('out')
        run(port_filter)

    def drums(self, normalize=1):
        normalize_counter = normalize
        port_split = []
        my_out_port = 1
        for key in range(36,55):
            port_filter = PortFilter(self.dev_name) \
                >> KeyFilter(key) \
                >> Transpose((36 - normalize_counter + normalize)) \
                >> Output(1, 1) \
                # >> Print(str(key))
            # Append port rule (or patch) to a list
            port_split.append(port_filter)
            print port_filter
            my_out_port += 1
            if normalize:
                normalize_counter += 1
        run(port_split)

if __name__ == "__main__":
    import sys
    print method_list(MidiFilter)
    m = MidiFilter(sys.argv[2])
    getattr(m,sys.argv[1])()

    # Usage: python midifilter.py drums|monitor <device name>
