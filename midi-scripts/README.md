# Midifilter

__Mididings based script that performs various actions on live midi streams.__

### Dependencies
##### Mididings

Mididings is very versatile, thefore quite heavy in dependencies.

__For Ubuntu 18.04:__
```
sudo apt install libasound2-dev libboost-python-dev libboost-thread-dev libsmf-dev python-all-dev python-gobject-2-dev libjack-dev
```

__Note__: if you have jack2 installed, use libjack-jackd2-dev:
```
sudo apt install libjack-jackd2-dev 
```


## Usage

* Find your device name:
  - `aplaymidi -l`

* Call script with function as first and device name as second arg

```
python midifilter.py drums|monitor|fix_velocity <device name>
```



### Methods

* monitor ( )
  - Outputs all messages on given midi port

* fix_velocity ( velocity: int )
  - Takes int between 1 and 127 and raises all incoming midi note to that

* drums ( normalize: int )
  - Transposes incoming notes



