# Using Drumpad with Linux

So I bought this Akai LPD8 ease the work on some projects. 
I was very excited about it but I had to realized none of my programs was able 
to correctly utilize it. 

## Problem
* I found only commercial software to be able to recognize my device and use it as a drumpad as I wish to.
* The Keys are ordered as a piano keyboard: __c1-c#1-d1-e1-e#1__... 
* This doesn't match the most Virtual Drumsets Keyboard layout

## Solution
I was looking for ways to circumvent that. 
One thing I found was the __soundfont__(*.sf2*) format which is a pretty open standard and one can simply create a soundfont. 
This world enable you to create a piano-like Instrument which exactly matches the pads your drumpad. 
This can either be done with [polyphone](http://polyphone-soundfonts.com/en/) or [swami](http://www.swamiproject.org/).  
See full list of _.sf2_-Editors [here](https://musescore.org/node/13734)

I found polyphone the easiest to use, so i tried this:
![polyphone interface](polyphone-dnb.png)

As you can see, i looked up which key comes with which note, and mapped these together. In my case, the pad controller maps the keys to notes with different base keys and intervals dependent on the program. 
Of course, this can be a huge benefit when you know how to use it.
But as i do not, i just ignored the program related things and kept in program 1.

Sadly, all this would mean a lot of configuration and uncomfortable adjustment of the sounds of samples while/before mapping them to a soundfont. 
I went through the process and decided it wouldn't be intuitive enough for me, so I looked deeper. 

## Mididings
After looking deeper I stumbled upon [mididings](http://das.nasophon.de/mididings/).  
It is a Python interface for MIDI devices.  
As Python is my favorite programming language I thought I should give it a try. 
The documentation took me a long time to think about and understand how to use it.
At the time of writing i am still not sure if I really understood.
Still I'm using it a lot and its great!

So first thing was reading the docs.
I realized that this was going deep into midi science, so i had to read some more:
* https://www.noterepeat.com/articles/how-to/213-midi-basics-common-terms-explained

### So what to do with mididings

To get this going I had to  

* Find my device Name and Port with `aplaymidi -l`. 
* Get the exact String Format so it's readable by mididings (*Sed, awk, grep*)
* Identify midi device with formatted name and port
* Create 8 output midi devices
* for each key on keyboard
  - Create list of filter instructions
    + Catch signals
    + Output signals to corresponding port

* Pass list to mididings `run` function, which executes the filter and keeps listening as long as the script is running.

And tada: While the "patch" is running, my alsa-midi devices lists shows 8 more devices: `LPD8_mididings_[1-8]`.  

![mididings-split](mididings-split.png)


#### Port-split.py

~~~python
from mididings import *
from subprocess import call
import os


# Define name to look for 
dev_name = "LPD8"
normalize = 1

# Define command strings to find device details
# Use 'aplaymidi', sed and grep to find and filter valid Client Name and Port
dev_port_filter = "aplaymidi -l|grep '%s'|awk '{print $1}'" % dev_name
dev_name_filter = "aplaymidi -l|grep LPD|awk '{for(i=3;i<=NF;++i)printf $i " "}'|sed -e 's; $;;g'".strip("\n")

# Get ouput of actual commands with os.popen
dev_num = os.popen(dev_port_filter).read().strip("\n")
dev_name = os.popen(dev_name_filter).read()


# Check if device port isnt empty, else, exit program
if dev_num is "":
    print "Oh! Device %s does not seem to be connected" % dev_name
    exit
print "Found device:", dev_num


# Call mididings method to configure the interface
config(
  backend='alsa', # should also work with jack
  client_name='LPD8_mididings', # Shown as device name
  # in_ports = [('LPD8 MIDI 1',24:0)],  # For manual setup
  in_ports = [(dev_name,dev_num)],  # find with aplaymidi -l
  out_ports = 8 # Number of ports to create. Also takes list of names.
)

# Explanation of mididings port filter sequence:
#
# Catch all keys (8) on keyboard
#   >> KeyFilter(key) \ 
#
# Transpose every one to "c4"
#   >> Transpose((36-out_port+1)) \
#
# Output to port client_name_c
#   >> Output(out_port, 1) \
#
# Print result for visual feedback
#   >> Print(str(dev_name, key))

# Now, for every key on our device create a filter object
# that routes the signal to our custom port
normalize_counter = normalize
port_split = []
my_out_port = 1
for key in range(36,44):
    # Catch device signals
    port_filter = PortFilter(dev_name) \
        >> KeyFilter(key) \
        >> Transpose((36 - normalize_counter + normalize)) \
        >> Output(my_out_port, 1) \
        >> Print(str(key))
    # Append port rule (or patch) to a list
    port_split.append(port_filter)
    print port_filter
    my_out_port += 1
    if normalize:
        normalize_counter += 1


# Call mididings run with our list of patches
run(port_split)
~~~
