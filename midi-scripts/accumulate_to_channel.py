from subprocess import call
from mididings import *
import os

dev_name = "e-drum MIDI 1"
dev_port_filter = "aplaymidi -l|grep '%s'|awk '{print $1}'" % dev_name
dev_name_filter = "aplaymidi -l|grep '%s'|awk '{for(i=3;i<=NF;++i)printf $i " "}'|sed -e 's; $;;g'".strip("\n")  % dev_name

dev_num = os.popen(dev_port_filter).read().strip("\n")
dev_name = os.popen(dev_name_filter).read()

config(
  backend='alsa', # should also work with jack
  client_name='E-DRUM', # Shown as device name
  in_ports = [(dev_name,dev_num)],  # find with aplaymidi -l
  out_ports = ["Channeltrans"] # 1 # Number of ports to create. Also takes list of names.
)

port_filter = PortFilter(dev_name) >> \
              Filter(NOTE|PROGRAM|AFTERTOUCH) >> \
              Velocity(fixed=127) >> \
              Print('in') >> \
              Output(1, 1) >> \
              Print('out') 
run(port_filter)
