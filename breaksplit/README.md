# BreakSplit

Simple script that splits a give .wav file into multiple, equally-sized 
chunks and exports them in rearranged order.


Usage:
```
python3 breaksplit.py [infile] [outfile] (number of chunks) (number of repetitions)

python3 breaksplit.py amen.wav new.wav 16 5
```
